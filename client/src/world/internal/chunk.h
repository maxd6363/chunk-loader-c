#ifndef CHUNK_H_
#define CHUNK_H_


#define CHUNCK_SIZE 16

typedef enum{BLOCK_NONE,BLOCK_STONE,BLOCK_GRASS,BLOCK_SAND,BLOCK_WATER}block_t;

typedef struct{
    int x;
    int y;
    block_t **data;
}chunk_t;



void chunkLoad(chunk_t **out, int x,int y);
void chunkFree(chunk_t **chunk);


#endif