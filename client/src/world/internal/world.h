#ifndef WORLD_H_
#define WORLD_H_

#include "chunk.h"

#define RENDER_DISTANCE 2

typedef struct {
	chunk_t **data;
	int renderDistance;
	int dataLength;
} world_t;

void worldInit(int renderDistance, world_t *out);
void worldLoad(int playerX, int playerY, world_t *out);



#endif