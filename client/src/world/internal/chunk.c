#include "chunk.h"

#include <stdlib.h>

void chunkLoad(chunk_t **out, int x, int y) {
	if (out) {
		*out = malloc(sizeof(chunk_t));
		if (*out) {
			(*out)->x = x;
			(*out)->y = y;
			(*out)->data = malloc(CHUNCK_SIZE * sizeof(block_t*));
			for (int i = 0; i < CHUNCK_SIZE; i++) {
				(*out)->data[i] = malloc(CHUNCK_SIZE * sizeof(block_t));
				for (int j = 0; j < CHUNCK_SIZE; j++) {
					//(*out)->data[i][j]=rand()%2;
					(*out)->data[i][j]=(i+j)%4+1;
				}
			}
		}
	}
}

void chunkFree(chunk_t **chunk) {
	if (chunk) {
		free((*chunk)->data);
		free(*chunk);
		*chunk = NULL;
	}
}
