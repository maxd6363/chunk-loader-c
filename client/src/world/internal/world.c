#include "world.h"

#include <stdlib.h>

void worldInit(int renderDistance, world_t *out) {
	out->renderDistance=renderDistance;
	out->dataLength=(renderDistance * 2 + 1)*(renderDistance * 2 + 1);
	out->data = malloc(out->dataLength * sizeof(chunk_t **));
}

void worldLoad(int playerX, int playerY, world_t *out) {
	int k = 0;
	for (int i = -out->renderDistance; i <= out->renderDistance; i++) {
		for (int j = -out->renderDistance; j <= out->renderDistance; j++) {
			chunkLoad(&out->data[k], playerX + i, playerY + j);
			k++;
		}
	}
}