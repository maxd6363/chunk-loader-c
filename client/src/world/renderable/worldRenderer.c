#include "worldRenderer.h"

void renderWorld(SDL_Renderer *renderer, world_t *world, camera_t *camera) {
	if (world && world->data) {
		for (int i = 0; i < world->dataLength; i++) {
			renderChunk(renderer, world->data[i], camera);
		}
	}
}

void renderChunk(SDL_Renderer *renderer, chunk_t *chunk, camera_t *camera) {
	if (chunk && chunk->data) {
		SDL_SetRenderDrawColor(renderer, (chunk->x * chunk->y) % 256, (chunk->x * chunk->y) % 256, (chunk->x * chunk->y) % 256, 255);
		SDL_Rect rect;
		rect.x = chunk->x * camera->tileSize * CHUNCK_SIZE;
		rect.y = chunk->y * camera->tileSize * CHUNCK_SIZE;
		rect.w = rect.h = camera->tileSize * CHUNCK_SIZE;
		SDL_RenderDrawRect(renderer, &rect);
	}
}