#include "itemRenderer.h"

#include <SDL2/SDL_image.h>

#include "../../utils/bool.h"
#include "../../utils/error.h"

struct {
	SDL_Texture *texture[MAX_ITEMS];
} itemAssets;

void renderItemInit(SDL_Renderer *renderer) {
	char tmp[128];
	for (int i = 0; i < MAX_ITEMS; i++) {
		sprintf(tmp, "%sitem%d.png", RENDER_PATH, i % 6 + 1);
		itemAssets.texture[i] = IMG_LoadTexture(renderer, tmp);
		if (!itemAssets.texture[i]) {
			logError(FILE_NOT_FOUND);
		}
	}
}

void renderItemFree() {
	for (int i = 0; i < MAX_ITEMS; i++) {
		SDL_DestroyTexture(itemAssets.texture[i]);
	}
}

void renderItem(SDL_Renderer *renderer, item_t *items) {
	for (int i = 0; i < MAX_ITEMS; i++) {
		if (items[i].exist) {
			SDL_Rect dest = {(int)items[i].x, (int)items[i].y, ITEM_WIDTH, ITEM_HEIGHT};
			SDL_RenderCopy(renderer, itemAssets.texture[i], NULL, &dest);
		}
	}
}
