#ifndef WORLDRENDERER_H_
#define WORLDRENDERER_H_

#include "../internal/world.h"
#include "../../graphics/camera.h"
#include <SDL2/SDL.h>

#define CHUNK_TILE_SIZE 2



void renderWorld(SDL_Renderer *renderer, world_t *world, camera_t *camera);
void renderChunk(SDL_Renderer *renderer, chunk_t *chunk, camera_t *camera);



#endif