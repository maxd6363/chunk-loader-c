#ifndef HEALTHBARRENDERER_H_
#define HEALTHBARRENDERER_H_

#include <SDL2/SDL.h>
#include "../internal/player.h"



#define SCALE_BAR 8
#define LENGHT_BAR 6
#define OFF_SET_X 5
#define OFF_SET_Y 30


void healthbarRender(SDL_Renderer *renderer, player_t *player);
void healthbarDrawRectangle(SDL_Renderer *renderer, SDL_Rect rect, int r, int g, int b,int a);


#endif