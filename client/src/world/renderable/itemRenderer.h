#ifndef ITEMRENDERER
#define ITEMRENDERER

#include <SDL2/SDL.h>

#include "../internal/item.h"

#define ITEM_RENDER_SPRITE_WIDTH 16
#define ITEM_RENDER_SPRITE_HEIGHT 20
#define RENDER_PATH "textures/item/"

void renderItemInit (SDL_Renderer * renderer);
void renderItemFree ();
void renderItem (SDL_Renderer * renderer, item_t * item);

#endif
