#include "game.h"

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

#include "graphics/camera.h"
#include "graphics/graphics.h"
#include "graphics/musicPlayer.h"
#include "process/input.h"
#include "server/server.h"
#include "title-screen.h"
#include "utils/bool.h"
#include "utils/color.h"
#include "utils/math.h"

#include "world/internal/player.h"
#include "world/internal/world.h"

#include "world/renderable/playerRenderer.h"
#include "world/renderable/worldRenderer.h"

long ticks = 0;
int frames = 0;

config_t gameConfig;

camera_t camera;
server_t server;

player_t player;

world_t world;

void gameSetup(config_t config) {
	srand(6);
	gameConfig = config;
}

void gameStart(SDL_Window *window) {
	bool running = true;
	bool halt = false;
	bool keyPressed;
	bool loadWorld = true;

	SDL_Renderer *renderer = SDL_GetRenderer(window);
	direction_t direction = DIR_NONE;
	int windowWidth, windowHeight;

	SDL_GetWindowSize(window, &windowWidth, &windowHeight);

	renderPlayerInit(renderer);

	server = serverConnect(gameConfig.server, gameConfig.port);
	if (!server.online) {
		gameEnd(window);
		return;
	}
	serverGetLaby(server, &gameConfig);
	srand(gameConfig.seed);
	serverGetPlayerInfo(server, &gameConfig);

	cameraInit(&camera, 2 + 2 + 1, gameConfig.playerID, windowHeight);
	playerInit(&player, 2, 2, camera.tileSize, gameConfig.playerID);

	worldInit(2, &world);
	worldLoad((int)player.x, (int)player.y, &world);

	while (running) {
		ticks = SDL_GetTicks();

		inputMisc(&halt, &running, &camera, &player, window, &windowWidth, &windowHeight, &loadWorld);
		if (loadWorld)
			worldLoad(player.x, player.y, &world);

		gameRenderGame(window);

		if (!halt) {
			playerStop(&player);
			inputPlayerMovement(&keyPressed, &direction);
			playerUpdateCollision(&player,direction,keyPressed);

			//cameraUpdate(&camera, renderer, &player, gameConfig.worldSize, windowWidth, windowHeight);
		}

		SDL_RenderPresent(renderer);
		SDL_Delay(SDL_GetTicks() - ticks < MS_SLEEP_WANTED ? MS_SLEEP_WANTED - (SDL_GetTicks() - ticks) : 0);
		frames++;
	}

	serverDisconnect(server);

	renderPlayerFree();

	gameEnd(window);
}

void gameEnd(SDL_Window *window) {
	printf("Game Ended %p\n", window);
}

void gameRenderGame(SDL_Window *window) {
	int windowWidth, windowHeight;
	SDL_GetWindowSize(window, &windowWidth, &windowHeight);
	SDL_Renderer *renderer = SDL_GetRenderer(window);
	gcsClearBackground(renderer, gameConfig.playerID);
	renderWorld(renderer, &world, &camera);
	renderPlayer(renderer, &player);
}

void gameSignalHandler(int signo) {
	if (signo == SIGPIPE || signo == SIGSEGV)
		printf(RED "[CLIENT] Server connection lost\n" RESET);
}
