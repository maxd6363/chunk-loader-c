#include "graph.h"

#include "../utils/math.h"
#include <math.h>

graphe_t *grapheInit(int size) {
	graphe_t *G = (graphe_t *)malloc(sizeof(graphe_t));
	if (!G) {
		return NULL;
	}
	G->nbNoeuds = size;
	G->nbArretes = 0;
	G->i = malloc(size * size * sizeof(int));
	G->j = malloc(size * size * sizeof(int));
	if (G->i == NULL || G->j == NULL) {
		free(G);
		return NULL;
	}
	return G;
}

void graphArreteInit(graphe_t *G, int i, int j) {
	G->i[G->nbArretes] = i;
	G->j[G->nbArretes] = j;
	(G->nbArretes)++;
}

graphe_t *graphCreer(int size) {
	graphe_t *G = grapheInit(size);
	for (int i = 0; i < G->nbNoeuds; i++) {
		for (int j = i + 1; j < G->nbNoeuds; j++) {
			if (rand() % 6 == 0) {
				graphArreteInit(G, i, j);
			}
		}
	}
	return G;
}

graphe_t *graphCreerPourLaby(int size) {
	graphe_t *G = grapheInit(size);
	int integerSqrt = sqrt(size);
	int indexCalculated;
	for (int i = 0; i < integerSqrt; i++) {
		for (int j = 0; j < integerSqrt; j++) {
			indexCalculated = array2Dto1D(i, j, integerSqrt);
			if (j < integerSqrt - 1)
				graphArreteInit(G, indexCalculated, indexCalculated + 1);
			if (i < integerSqrt - 1)
				graphArreteInit(G, indexCalculated, indexCalculated + integerSqrt);
		}
	}
	return G;
}

void graphLiberer(graphe_t **G) {
	if (G) {
		if ((*G)->i)
			free((*G)->i);
		if ((*G)->j)
			free((*G)->j);
		free(*G);
	}
}

partition_t graphConnexes(graphe_t *G) {
	partition_t P = partitionCreer(G->nbNoeuds);
	for (int k = 0; k < G->nbArretes; k++) {
		if (partitionRecupererClasse(&P, G->i[k]) != partitionRecupererClasse(&P, G->j[k])) {
			partitionFusion(&P, G->i[k], G->j[k]);
		}
	}
	return P;
}

void graphDebug(graphe_t *G) {
	printf("Graph : \n");
	printf("\t Number of nodes : %d\n", G->nbNoeuds);
	printf("\t Number of links : %d\n", G->nbArretes);
}

void graphDisplay(graphe_t *G) {
	char tmp[1024];
	char filename[128];
	int random = rand();
	FILE *flot;

	sprintf(filename, "graph_%d.dot", random);

	if (G) {
		flot = fopen(filename, "w");
		fprintf(flot, "graph MyGraph {\n");
		for (int i = 0; i < G->nbNoeuds; i++) {
			fprintf(flot, "    %d;\n", i);
		}
		for (int k = 0; k < G->nbArretes; k++) {
			fprintf(flot, "    %d -- %d;\n", G->i[k], G->j[k]);
		}
		fprintf(flot, "}\n");
		fclose(flot);

		sprintf(tmp, "dot graph_%d.dot -T" IMAGE_FORMAT " > graph_%d." IMAGE_FORMAT " ; " IMAGE_VIEWER " graph_%d." IMAGE_FORMAT, random, random, random);
		system(tmp);
		system("rm *.dot *." IMAGE_FORMAT);
	}
}

graphe_t *kruskal(graphe_t *g, void (*f)(graphe_t *), double proba) {
	graphe_t *g2 = grapheInit(g->nbNoeuds);
	if ((*f) != NULL) {
		(*f)(g);
	}
	partition_t p = partitionCreer(g->nbNoeuds);
	int k;
	for (k = 0; k < g->nbArretes; k++) {
		if (partitionRecupererClasse(&p, g->i[k]) != partitionRecupererClasse(&p, g->j[k])) {
			partitionFusion(&p, g->i[k], g->j[k]); // Fusion des partitions
			graphArreteInit(g2, g->i[k], g->j[k]); // Ajouter l'arête au graphe 2
		} else {
			if ((float)rand() / (float)(RAND_MAX) < proba) {
				partitionFusion(&p, g->i[k], g->j[k]); // Fusion des partitions
				graphArreteInit(g2, g->i[k], g->j[k]); // Ajouter l'arête au graphe 2
			}
		}
	}
	partitionLiberer(&p);
	return g2;
}

void fisherYates(graphe_t *G) {
	for (int k = G->nbArretes - 1; k >= 1; k--) {
		int l = rand() % k;
		int tmp_i = G->i[k], tmp_j = G->j[k];
		G->i[k] = G->i[l];
		G->j[k] = G->j[l];
		G->i[l] = tmp_i;
		G->j[l] = tmp_j;
	}
}

void graphDeleteWalls(graphe_t *G, int s) {
	if (s >= 0) {
		if (s % (int)sqrt(G->nbNoeuds) != 0) {
			graphArreteInit(G, s, s - 1);
		}
		if (s % (int)sqrt(G->nbNoeuds) != (int)sqrt(G->nbNoeuds) - 1) {
			graphArreteInit(G, s, s + 1);
		}
		if (s > (int)sqrt(G->nbNoeuds) - 1) {
			graphArreteInit(G, s, s - (int)sqrt(G->nbNoeuds));
		}
		if (s < G->nbNoeuds - (int)sqrt(G->nbNoeuds) - 1) {
			graphArreteInit(G, s, s + (int)sqrt(G->nbNoeuds));
		}
	}
}

void graphCreateRoom(graphe_t *G, int numberOfRooms) {
	for (int i = 0; i < numberOfRooms; i++) {
		int s = rand() % G->nbNoeuds;
		graphDeleteWalls(G, s);
		graphDeleteWalls(G, s - 1);
		graphDeleteWalls(G, s + 1);
		graphDeleteWalls(G, s - (int)sqrt(G->nbNoeuds));
		graphDeleteWalls(G, s + (int)sqrt(G->nbNoeuds));
	}
}