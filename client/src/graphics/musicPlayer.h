#ifndef MUSICPLAYER_H_
#define MUSICPLAYER_H_

#define PATH_TO_MAIN_THEME "audio/musics/sanctuary.mp3"
#define PATH_TO_DAMAGE_GHOST "audio/sounds/ghost-damage.wav"
#define PATH_TO_DAMAGE_PLAYER_1 "audio/sounds/damage-player1.wav"
#define PATH_TO_DAMAGE_PLAYER_2 "audio/sounds/damage-player2.wav"
#define PATH_TO_DAMAGE_PLAYER_3 "audio/sounds/damage-player3.wav"
#define PATH_TO_AMBIANT_GHOST "audio/sounds/ghost-ambiant.wav"

#define PATH_TO_GAMEOVER "audio/sounds/gameover.wav"
#define PATH_TO_BOO_EFFECT "audio/sounds/boo-effect.wav"

#define AMBIANT_COOL_DOWN 3000

#define VOLUME_STEP 2
#define LOUDER_SOUND 4

#include "../utils/error.h"

error_t musicPlayerInit(void);
void musicPlay(void);
void musicPause(void);
void musicPlayerFree(void);
void musicVolumeDown(void);
void musicVolumeUp(void);
void musicVolumeMute(void);
void musicVolumeZero(void);
void musicSetSoundVolume(int vol);

void musicPlayDamage(int playerID);
void musicPlayGameover(void);
void musicPlayBoo(void);
void musicPlayAmbiante(void);

#endif
