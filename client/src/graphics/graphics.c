#include "graphics.h"

#include "../utils/error.h"
#include "../world/internal/player.h"

#include <SDL2/SDL_audio.h>
#include <SDL2/SDL_mixer.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

error_t gcsInit(void) {
	error_t error = OK;
    if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS | SDL_INIT_AUDIO) != 0)
		error = SDL_INIT_FAILED;
    if (IMG_Init(IMG_INIT_PNG) == 0)
		error = SDL_INIT_FAILED;
    if (Mix_Init(MIX_INIT_MP3) == 0)
	    error = SDL_INIT_FAILED;
    if (TTF_Init() < 0)
    {
        error = SDL_INIT_FAILED;
    }

	return error;
}

void gcsFree(void) {
	SDL_Quit();
	Mix_Quit();
	IMG_Quit();
	TTF_Quit();
}

error_t gcsNewWindow(const char *title, int width, int height, SDL_Window **out) {
	error_t error = OK;
	*out = SDL_CreateWindow(title, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height, SDL_WINDOW_RESIZABLE);
	if (*out == NULL) {
		error = SDL_WINDOW_FAILED;
		logError(SDL_WINDOW_FAILED);
	}
	SDL_Surface *icon = IMG_Load(PATH_TO_ICON);
	SDL_SetWindowIcon(*out,icon);
	return error;
}

void gcsFreeWindow(SDL_Window **window) {
	SDL_DestroyWindow(*window);
	*window = NULL;
}

error_t gcsNewRenderer(SDL_Window *window, SDL_Renderer **out) {
	error_t error = OK;
	*out = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
	if (*out == NULL) {
		error = SDL_ERROR;
		logError(SDL_ERROR);
	}
	return error;
}

void gcsFreeRenderer(SDL_Renderer **renderer) {
	SDL_DestroyRenderer(*renderer);
	*renderer = NULL;
}

void gcsClearBackground(SDL_Renderer *renderer, int playerID) {
	if(playerID == PLAYER_ID_GHOST)
		SDL_SetRenderDrawColor(renderer, 54, 54, 54, 0);
	else
		SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
	SDL_RenderClear(renderer);
}
