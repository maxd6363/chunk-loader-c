#include "musicPlayer.h"

#include "../utils/bool.h"
#include "../utils/math.h"
#include "../world/internal/player.h"
#include <SDL2/SDL.h>
#include <SDL2/SDL_mixer.h>

struct {
	Mix_Music *music;
	Mix_Chunk *gameover;
	Mix_Chunk *damagePlayer[3];
	Mix_Chunk *damageGhost[1];
	Mix_Chunk *ambiantGhost[1];
	Mix_Chunk *boo[1];
} musicPlayer;

int volume = MIX_MAX_VOLUME;
bool muted = false;


error_t musicPlayerInit(void) {

	if (Mix_OpenAudio(22050, MIX_DEFAULT_FORMAT, MIX_DEFAULT_CHANNELS, 1024) == -1) {
		logError(SDL_ERROR);
		return SDL_ERROR;
	}
	Mix_AllocateChannels(4);
	musicPlayer.music = Mix_LoadMUS(PATH_TO_MAIN_THEME);
	musicPlayer.gameover = Mix_LoadWAV(PATH_TO_GAMEOVER);
	musicPlayer.damagePlayer[0] = Mix_LoadWAV(PATH_TO_DAMAGE_PLAYER_1);
	musicPlayer.damagePlayer[1] = Mix_LoadWAV(PATH_TO_DAMAGE_PLAYER_2);
	musicPlayer.damagePlayer[2] = Mix_LoadWAV(PATH_TO_DAMAGE_PLAYER_3);

	musicPlayer.damageGhost[0] = Mix_LoadWAV(PATH_TO_DAMAGE_GHOST);

	musicPlayer.ambiantGhost[0] = Mix_LoadWAV(PATH_TO_AMBIANT_GHOST);

	musicPlayer.boo[0] = Mix_LoadWAV(PATH_TO_BOO_EFFECT);

	volume = 2;
	Mix_VolumeMusic(volume);
    musicSetSoundVolume(volume);
	return OK;
}

void musicPlay(void) {
	Mix_PlayMusic(musicPlayer.music, -1);
}

void musicPause(void) {
	Mix_PauseMusic();
}

void musicPlayerFree(void) {
	Mix_FreeMusic(musicPlayer.music);

	Mix_FreeChunk(musicPlayer.gameover);
	Mix_FreeChunk(musicPlayer.damagePlayer[0]);
	Mix_FreeChunk(musicPlayer.damagePlayer[1]);
	Mix_FreeChunk(musicPlayer.damagePlayer[2]);
	Mix_FreeChunk(musicPlayer.damageGhost[0]);
	Mix_FreeChunk(musicPlayer.ambiantGhost[0]);
	Mix_FreeChunk(musicPlayer.boo[0]);

	Mix_CloseAudio();
}

void musicVolumeDown(void) {
	volume = clamp(volume - VOLUME_STEP, 0, MIX_MAX_VOLUME);
	Mix_VolumeMusic(volume);
    musicSetSoundVolume(volume);
}

void musicVolumeUp(void) {
	volume = clamp(volume + VOLUME_STEP, 0, MIX_MAX_VOLUME);
	Mix_VolumeMusic(volume);
    musicSetSoundVolume(volume);
}

void musicVolumeMute(void) {
	muted = !muted;
	Mix_VolumeMusic(muted ? 0 : volume);
    musicSetSoundVolume(muted? 0 : volume);
}

void musicVolumeZero(void) {
	Mix_VolumeMusic(0);
    musicSetSoundVolume(0);
}

void musicSetSoundVolume(int vol)
{
    vol = vol * LOUDER_SOUND;
    Mix_VolumeChunk(musicPlayer.boo[0], vol);
    Mix_VolumeChunk(musicPlayer.gameover, vol);
    Mix_VolumeChunk(musicPlayer.ambiantGhost[0], vol);
    Mix_VolumeChunk(musicPlayer.damageGhost[0], vol);
    Mix_VolumeChunk(musicPlayer.damagePlayer[0], vol);
    Mix_VolumeChunk(musicPlayer.damagePlayer[1], vol);
    Mix_VolumeChunk(musicPlayer.damagePlayer[2], vol);
}

void musicPlayDamage(int playerID) {
	if (!Mix_Playing(0)) {
		if (playerID == PLAYER_ID_GHOST)
			Mix_PlayChannel(0, musicPlayer.damageGhost[0], 0);
		else
			Mix_PlayChannel(0, musicPlayer.damagePlayer[SDL_GetTicks() % 3], 0);
	}
}

void musicPlayGameover(void) {
	if (!Mix_Playing(1))
		Mix_PlayChannel(1, musicPlayer.gameover, 0);
}

void musicPlayBoo(void) {
	if (!Mix_Playing(2))
		Mix_PlayChannel(2, musicPlayer.boo[0], 0);
}

void musicPlayAmbiante(void) {
	if (!Mix_Playing(3)) {
		Mix_PlayChannel(3, musicPlayer.ambiantGhost[0], 0);
	}
}
