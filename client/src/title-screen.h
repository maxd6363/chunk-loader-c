#ifndef TITLE_SCREEN
#define TITLE_SCREEN

#include <SDL2/SDL.h>

#include "utils/bool.h"

#define PATH_TITLE_SCREEN "textures/player/player0.png"



bool gameTitle(SDL_Window * window);

SDL_Texture * chargerTexture (const char * nomImg, SDL_Renderer * renderer);

SDL_Texture * chargerTexte(const char * txt, const char * nomPolice, int taille, SDL_Color color, SDL_Renderer * renderer);

#endif
