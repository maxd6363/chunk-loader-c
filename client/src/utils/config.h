#ifndef CONFIG_H
#define CONFIG_H

typedef struct {
	int worldSize;
	char server[128];
	int port;
	int playerID;
	int seed;
	double percentKruskal;
	int numberOfRoom;
	int spawnX;
	int spawnY;
} config_t;

config_t configNew(int worldSize, const char *server, int port);
void configPrint(config_t config);

#endif