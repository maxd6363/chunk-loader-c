#include "error.h"
#include "color.h"
#include <stdio.h>

/**
 * @brief      Fonction pour logger les erreurs
 *
 * @param[in]  err   L'erreur
 */
void logError(error_t err) {
#ifdef VERBOSE
	switch (err) {
	case OK:
		fprintf(stderr, GRE "OK");
		break;
	case ERROR:
		fprintf(stderr, RED "ERROR : Erreur non spécifié");
		break;
	case FILE_NOT_FOUND:
		fprintf(stderr, RED "ERROR : Fichier non trouvé");
		break;
	case ALLOC_ERROR:
		fprintf(stderr, RED "ERROR : Erreur allocation");
		break;
	case OUT_OF_BOUNDS:
		fprintf(stderr, RED "ERROR : Index hors limite");
		break;
	case NULL_POINTER:
		fprintf(stderr, RED "ERROR : Pointer null");
		break;
	case NOT_IMPLEMENTED_YET:
		fprintf(stderr, RED "ERROR : Pas encore implementé");
		break;
	case STACK_OVERFLOW:
		fprintf(stderr, RED "ERROR : La pile est pleine");
		break;
	case STACK_EMPTY:
		fprintf(stderr, RED "ERROR : La pile est vide");
		break;
	case QUEUE_OVERFLOW:
		fprintf(stderr, RED "ERROR : La file est pleine");
		break;
	case QUEUE_EMPTY:
		fprintf(stderr, RED "ERROR : La file est vide");
		break;
	case WRONG_APP_VERSION:
		fprintf(stderr, RED "ERROR : Mauvaise version de l'application");
		break;

	case SDL_ERROR:
		fprintf(stderr, RED "ERROR : SDL erreur");
		break;
	case SDL_INIT_FAILED:
		fprintf(stderr, RED "ERROR : SDL Init erreur");
		break;
	case SDL_WINDOW_FAILED:
		fprintf(stderr, RED "ERROR : SDL Window erreur");
		break;
	case ASSETS_LOADING_FAILED:
		fprintf(stderr, RED "ERROR : SDL Loading assets error");
		break;

	default:
		fprintf(stderr, RED "ERROR");
		break;
	}
	printf("\n" RESET);
#endif
}
