#include "bool.h"

#include <stdio.h>
#include <stdlib.h>

void printBoolArray(bool * array, int size){
    if(array){
        for (int i = 0; i < size; i++) {
           if(array[i]){
               printf("%d  ",i );
           }
        }
        printf("\n");
    }
}


bool *boolAlloc(void){
    return malloc(sizeof(bool));
}