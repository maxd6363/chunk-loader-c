#include "collisionHandler.h"

void collisionGenerateRects(SDL_Rect *out, int *numberOfRect, labyrinthe_t *laby, player_t *player, camera_t *camera) {
	if (out && numberOfRect && laby && player && camera) {
		player->yWorldPosition = player->y / camera->tileSize;
		player->xWorldPosition = player->x / camera->tileSize;
		*numberOfRect = 0;

		for (int i = -1; i <= 1; i++) {
			for (int j = -1; j <= 1; j++) {
				if (player->xWorldPosition + i >= 0 && player->xWorldPosition + i < laby->size && player->yWorldPosition + j >= 0 && player->yWorldPosition + j < laby->size) {
					if (isTile(laby->data[player->yWorldPosition + j][player->xWorldPosition + i], TILE_CLOSED_DOWN)) {
						out[*numberOfRect].x = (player->xWorldPosition + i) * camera->tileSize;
						out[*numberOfRect].y = (player->yWorldPosition + 1 + j) * camera->tileSize;
						out[*numberOfRect].w = camera->tileSize;
						out[*numberOfRect].h = 5;
						(*numberOfRect)++;
					}
					if (isTile(laby->data[player->yWorldPosition + j][player->xWorldPosition + i], TILE_CLOSED_UP)) {
						out[*numberOfRect].x = (player->xWorldPosition + i) * camera->tileSize;
						out[*numberOfRect].y = (player->yWorldPosition + j) * camera->tileSize;
						out[*numberOfRect].w = camera->tileSize;
						out[*numberOfRect].h = camera->tileSize / 2;
						(*numberOfRect)++;
					}
					if (isTile(laby->data[player->yWorldPosition + j][player->xWorldPosition + i], TILE_CLOSED_LEFT)) {
						out[*numberOfRect].x = (player->xWorldPosition + i) * camera->tileSize;
						out[*numberOfRect].y = (player->yWorldPosition + j) * camera->tileSize;
						out[*numberOfRect].w = (int)(0.12 * camera->tileSize);
						out[*numberOfRect].h = camera->tileSize;
						(*numberOfRect)++;
					}
					if (isTile(laby->data[player->yWorldPosition + j][player->xWorldPosition + i], TILE_CLOSED_RIGHT)) {
						out[*numberOfRect].x = (player->xWorldPosition + 1 + i) * camera->tileSize - (int)(0.12 * camera->tileSize);
						out[*numberOfRect].y = (player->yWorldPosition + j) * camera->tileSize;
						out[*numberOfRect].w = (int)(0.12 * camera->tileSize);
						out[*numberOfRect].h = camera->tileSize;
						(*numberOfRect)++;
					}
				}
			}
		}
	}
}

bool collidedRectWithRects(SDL_Rect *rects, int numberOfRect, SDL_Rect *playerRect, direction_t direction, camera_t *camera, int playerID) {
	if (rects && playerRect) {
		switch (direction) {
		case DIR_DOWN:
			playerRect->y += PLAYER_SPEED * camera->tileSize * (playerID == PLAYER_ID_GHOST ? PLAYER_GHOST_SPEED_BOOST : 1.0);
			break;
		case DIR_UP:
			playerRect->y -= PLAYER_SPEED * camera->tileSize * (playerID == PLAYER_ID_GHOST ? PLAYER_GHOST_SPEED_BOOST : 1.0);
			break;
		case DIR_RIGHT:
			playerRect->x += PLAYER_SPEED * camera->tileSize * (playerID == PLAYER_ID_GHOST ? PLAYER_GHOST_SPEED_BOOST : 1.0);
			break;
		case DIR_LEFT:
			playerRect->x -= PLAYER_SPEED * camera->tileSize * (playerID == PLAYER_ID_GHOST ? PLAYER_GHOST_SPEED_BOOST : 1.0);
			break;
		case DIR_NONE:
			break;
		}
	}
	for (int h = 0; h < numberOfRect; h++) {
		if (SDL_HasIntersection(playerRect, &rects[h])) {
			return true;
		}
	}
	return false;
}