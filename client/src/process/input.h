#ifndef INPUT_H_
#define INPUT_H_

#include <SDL2/SDL.h>

#include "../utils/bool.h"
#include "../utils/direction.h"
#include "../graphics/camera.h"
#include "../world/internal/player.h"



void inputPlayerMovement(bool * keyPressed, direction_t * direction);
void inputMisc(bool * halt, bool *running, camera_t *camera, player_t *player, SDL_Window * window, int *windowWidth,int *windowHeight, bool *loadWorld);


#endif