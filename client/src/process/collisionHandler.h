#ifndef COLLISIONHANDLER_H_
#define COLLISIONHANDLER_H_

#include "../utils/bool.h"
#include "../world/internal/labyrinthe.h"
#include "../world/internal/player.h"

#include <SDL2/SDL.h>

#define COLLISION_MAX_WALLS_TO_CHECK 36

void collisionGenerateRects(SDL_Rect *out, int *numberOfRect, labyrinthe_t *laby, player_t *player, camera_t *camera);
bool collidedRectWithRects(SDL_Rect *rects,int numberOfRect, SDL_Rect *player,direction_t direction, camera_t *camera, int playerID);



#endif