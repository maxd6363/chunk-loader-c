#ifndef DIRECTION_H_
#define DIRECTION_H_

typedef enum { DIR_NONE,
			   DIR_UP,
			   DIR_DOWN,
			   DIR_LEFT,
			   DIR_RIGHT } direction_t;

#endif