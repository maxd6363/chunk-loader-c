#include <signal.h>
#include <stdio.h>
#include <stdlib.h>

#include "server.h"

#define DEFAULT_PORT 25565
#define DEFAULT_SIZE 13
#define DEFAULT_PERCENT_KRUSKAL 0.04
#define DEFAULT_CHANCE_ROOM 3

int main(int argc, char const *argv[]) {
	int port = DEFAULT_PORT;
	int worldSize = DEFAULT_SIZE;

	if (argc != 3 && argc != 1) {
		printf("Usage: %s \n", argv[0]);
		printf("Usage: %s <port> <world size>\n", argv[0]);
		return 1;
	}
	if (argc == 3) {
		port = atoi(argv[1]);
		worldSize = atoi(argv[2]);
	}

	if (signal(SIGPIPE, signalHandler) == SIG_ERR) {
		serverSpeak(error,"Can't attach to signal interruption");
	}

	serverSetup(port, worldSize * worldSize, DEFAULT_PERCENT_KRUSKAL, worldSize / DEFAULT_CHANCE_ROOM);
	serverStart();

	return 0;
}
