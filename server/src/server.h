#ifndef SERVER_H_
#define SERVER_H_

#define MAX_CLIENTS 5
#define MAX_ITEMS 5


#define LONG_BUFFER 86
#define SHORT_BUFFER 8



#include "utils/bool.h"

typedef enum {
	ok,
	error,
	warning
} message_type_t;


void serverSetup(int port,int worldSize, double percentKruskal,int numberOfRooms);
void serverStart(void);
void *handleClient(int *sd);
bool sendLaby(int sd, int idThread);
bool sendAppVersion(int sd, int idThread);
bool sendPlayerInfo(int sd, int idThread);

bool sendGameState(int sd, int idThread);
bool getGameState(int sd, int idThread);



void signalHandler(int signo);
void serverSpeak(message_type_t type, const char *message, ...);
void threadSpeak(int id, message_type_t type, const char *message, ...);

#endif