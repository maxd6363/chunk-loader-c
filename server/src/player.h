#ifndef PLAYER_H_
#define PLAYER_H_

#include "utils/bool.h"

typedef struct {
	float health;
	float x;
	float y;
	int w;
	int h;
	int id;
	int direction;
	bool moving;
	bool lookingLeft;
	int xWorldPosition;
	int yWorldPosition;
	bool connected;
	bool usingSpecialPower;
	int lastUsedSpecialPower;
	unsigned int cdPower;
} player_t;

void playerSerialize(player_t *player, char *out);
void playerDeserialize(player_t *out, char *data);

#endif